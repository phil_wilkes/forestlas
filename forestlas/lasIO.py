import os
import struct
import datetime
import tempfile
import shutil
import subprocess
import numpy as np
import math
from lasStructure import *


class lasIO:

    """
    This allows reading and writing of .las files (currently supports 1.1 and 1.2).
    .las files can be read as one file(s), gridded or a plot of data extracted. 
    
    Output types include .las, a numpy array or as .znr file where only the height
    and the "number of returns" metadata remain. 
    """

    def __init__(self, path, out=False, verbose=False, search=".",
                 temp_dir=False, keep_temp=False, number_of_processes=1,
                 copy=False, create_temp=True):

        """
        Functions creates holder for .las file and setups required dictionaries
        etc.
        
        Parameters
        ----------
        path: File path or list
            file path to tile .las file or directory containing .las files,
            also excepts a list of file paths.
        
        out: Path to directory or path to save .las to, Default None
            If None then saves output to os.getcwd()

        search: String. Default "."
            Can be used to filter .las files if a directory is supplied for
            "path"
        
        temp_dir: File path to temporary directory. Default False
            Multiprocessing is not available with this module but is by others 
            that call lasIO.  This specifies the temporary directory.
        
        keep_temp: Boolean. Default True
            When False all temporary files are kept.  It is important to run 
            removeTemp() to clean up before exiting

        number_of_processes: Int, Default 1
            If processing a number of tiles or using a regular grid, Python's
            multiprocessing canbe envoked.  This variable sets the number of
            cores that are utilised.

        copy: Boolean, Default False
            Wether to copy files to the temp directory before processing.  This
            can speed thinks up if reading a number of large files simultaneously
            from a remote drive.
        
        Returns
        -------
        out: self
         
        """

        self.verbose = verbose
        self.keep_temp = keep_temp
        self.number_of_processes = number_of_processes

        # parse args and create file structure
        # is path a directory or file
        if isinstance(path, list):
            self.tile_list = path
            self.dir = os.path.split(path[0])[0]
            self.tile = None
        elif os.path.isfile(path):
            self.dir = os.path.split(path)[0]
            if self.dir == '': self.dir = os.getcwd()
            self.tile = os.path.split(path)[1]
            self.tile_list = [os.path.join(self.dir, self.tile)]
        elif os.path.isdir(path):
            self.dir = path
            self.tile = None
            tile_list = os.listdir(self.dir)
            self.tile_list = [tile for tile in tile_list if (tile.endswith(".las") or
                                                             tile.endswith(".laz")) and
                                                             tile.find(search) != -1]
        else:
            raise NameError("path not recognised")

        if len(self.tile_list) == 0:
            raise IOError("There are no .las or .laz tiles in {}".format(path))

        # create temporary directory in %temp%
        if create_temp:
            if temp_dir:
                self.tempDirectory = temp_dir
                if os.path.isdir(self.tempDirectory) is False:
                    os.makedirs(self.tempDirectory)
            else:
                temp_dir_name = "lidar.processing." + str(np.random.randint(0, 9999999)) + ".tmp"
                self.tempDirectory = os.path.join(tempfile.gettempdir(), temp_dir_name)
                os.makedirs(self.tempDirectory)

        if self.verbose: print "temporary directory at: {}".format(self.tempDirectory)

        # create output directory
        if not out:
            self.savePath = self.dir
            self.saveLAS = None
        elif os.path.isdir(out):
            self.savePath = out
            self.saveLAS = None
        else:
            self.savePath = os.path.split(out)[0]
            self.saveLAS = os.path.split(out)[1]

        # global variables    
        self.numberTiles = 1
        for i, las in enumerate(self.tile_list):
            las = os.path.join(self.dir, las)
            if i == 0:
                self.global_header = parseHeader(las)
                # self.global_header["numptrecords"] = las_header["numptrecords"]
                if self.global_header['filesig'] == "ZNRF":
                    raise Exception('ZNR is deprecated, use an older version of lasIO')
                else:
                    self.point_format, self.dt = self.getpoint_format()
                self.vlr = self.getVLR(las)
                # h = parseHeader(las)
            else:
                las_header = parseHeader(os.path.join(path, las))
                self.global_header["numptrecords"] += las_header["numptrecords"]
                if las_header["xmax"] > self.global_header["xmax"]:
                    self.global_header["xmax"] = las_header["xmax"]
                if las_header["xmin"] < self.global_header["xmin"]:
                    self.global_header["xmin"] = las_header["xmin"]
                if las_header["ymax"] > self.global_header["ymax"]:
                    self.global_header["ymax"] = las_header["ymax"]
                if las_header["ymin"] < self.global_header["ymin"]:
                    self.global_header["ymin"] = las_header["ymin"]
                self.numberTiles += 1

        self.x_centre = np.mean([self.global_header["xmax"], self.global_header["xmin"]])
        self.y_centre = np.mean([self.global_header["ymax"], self.global_header["ymin"]])

        if self.verbose: print "number of tiles to process:", self.numberTiles
        if self.numberTiles > 1 and self.verbose: print "processing tiles from:", self.dir

        # admin!
        self.counter = self.global_header["numptrecords"] // 20
        if self.global_header["numptrecords"] > 1e7:
            self.counter = self.global_header["numptrecords"] // 100
        self.badPoints = 0
        self.copy = copy
        if self.copy and self.verbose: print 'tiles are copied to temp'
        self.resolution = None

    def all(self, take_sample=False):

        """
        Returns complete .las file

        Parameters
        ----------

        take_sample: int, Default False
            Samples data at specified factor

        Returns
        -------
        out: self

        """

        total_points = 0
        self.global_header["guid1"] = 0

        x, y = self.global_header["xmin"], self.global_header["ymin"]
        if self.saveLAS is None:
            self.saveLAS = str(int(x)) + "_" + str(int(y)) + "_OUT.las"
        self.xy_dictionary = {}
        self.xy_dictionary['all'] = { "xmin":x,
                                      "ymin":y,
                                      "zmin":99999,
                                      "xmax":self.global_header["xmax"],
                                      "ymax":self.global_header["ymax"],
                                      "zmax": -999,
                                      "num_rtn": {1:0, 2:0, 3:0, 4:0, 5:0},
                                      "i":0,
                                      "outFile": os.path.abspath(os.path.join(self.savePath, self.saveLAS)),
                                      "tempFile": os.path.abspath(os.path.join(self.tempDirectory, str(x) + "_" + str(y) + ".temp"))}

        for tile in self.tile_list:

            if len(os.path.split(tile)) > 1:
                    tile = os.path.join(self.dir, tile)

            if self.copy:
                shutil.copyfile(tile, os.path.join(self.tempDirectory, os.path.split(tile)[1]))
                tile = os.path.join(self.tempDirectory, os.path.split(tile)[1])
            h = parseHeader(tile)

            if h["filesig"] == "LASF":
                if h["guid2"] == 1:
                    tile = self.laz2las(tile)

            if take_sample:
                sample = self.generateSample(take_sample, h)
                if self.verbose: print "random sample produced: {}".format(len(sample))
            else:
                sample = range(h['numptrecords'])

            with open(os.path.join(self.dir, tile), 'rb') as fh:

                fh.seek(h["offset"])

                for i in sample: # loops through all points

                    if self.verbose and total_points % self.counter == 0:
                        print "{:.0f}% | {} of {} points processed | {}".format((total_points / float(self.global_header['numptrecords'])) * 100,
                                                                                      total_points, self.global_header['numptrecords'],
                                                                                      datetime.datetime.now())

                    fh.seek(h["offset"] + (i * h['pointreclen'])) # searches to beginning of point
                    self.xy_dictionary["all"] = self.write_point(self.xy_dictionary["all"], self.extract_return(fh), h)
                    total_points += 1

        return self

    def plot(self, centre_x, centre_y, extent=24, round=False,
             take_sample=False):
        """

        Returns a plotwise array of points from the tile defined with
        lasIO with plot centre at centre_x and centre_y and area equal
        to (radius*2)**2 if round=False and pi*radius**2 if round=True.

        The radius defaults to 10 metres. Plots are square by default
        but can be circular with round=True.

        Returns self which returns a numpy array if asArray is cploted,
        or can saved as .las, .txt or .xyz by cploting exportLAS,
        exportTXT or exportXYZ respectively.

        Paramaters
        ----------
        centre_x, centre_y : int or float
            Cartesian coordinates of plot centre (in the same coordinate
        system as data)

        extent : int, float or tuple with length of 2
            Diameter of round plot or extent of square plot.  Will except
            a tuple of two ints or floats.

        round : Boolean, defualt False
            If False a square plot is returned, if True a round plot is
        returned

        Returns
        -------
        out : self

        """

        if isinstance(extent, tuple):
            extent_x = extent[0] / 2.
            extent_y = extent[1] / 2.
        else:
            extent_x, extent_y = extent / 2., extent / 2.

        xmin = centre_x - extent_x
        xmax = centre_x + extent_x
        ymin = centre_y - extent_y
        ymax = centre_y + extent_y

        total_points = 0
        self.global_header["guid1"] = 0

        x, y = self.global_header["xmin"], self.global_header["ymin"]
        if self.saveLAS is None:
            self.saveLAS = str(int(x)) + "_" + str(int(y)) + "_OUT.las"
        self.xy_dictionary = {}
        self.xy_dictionary['plot'] = {"xmin":xmin,
                                      "ymin":ymin,
                                      "zmin":999,
                                      "xmax":xmax,
                                      "ymax":ymax,
                                      "zmax": -999,
                                      "num_rtn": {1:0, 2:0, 3:0, 4:0, 5:0},
                                      "i":0,
                                      "outFile": os.path.abspath(os.path.join(self.savePath, self.saveLAS)),
                                      "tempFile": os.path.abspath(os.path.join(self.tempDirectory, str(x) + "_" + str(y) + ".PLOT.temp"))}

        for tile in self.tile_list:

            tile = os.path.join(self.dir, tile)
            h = parseHeader(tile)
            self.tile = tile

            if h["xmax"] < xmin or h["xmin"] > xmax or h["ymax"] < ymin or h["ymin"] > ymax:
                continue

            if h["filesig"] == "LASF":
                if h["guid2"] == 1:
                    tile = self.laz2las(tile)

            # h["num_rtn"] = {}

            if take_sample:
                sample = self.generateSample(take_sample, h)
                if self.verbose: print "random sample produced: {}".format(len(sample))
            else:
                sample = range(h['numptrecords'])

            with open(tile, 'rb') as fh:

                fh.seek(h["offset"])

                for i in sample: # loops through plot points

                    total_points += 1
                    if total_points % self.counter == 0 and self.verbose:
                        print "{:.0f}% | {} of {} points processed | {}".format((total_points / float(self.global_header['numptrecords'])) * 100,
                                                                                      total_points, self.global_header['numptrecords'],
                                                                                      datetime.datetime.now())

                    fh.seek(h["offset"] + (i * h['pointreclen'])) # searches to beginning of point

                    # test x point first ...
                    fh.seek(h['offset'] + (i * h['pointreclen']))
                    x = fh.read(4)
                    x = struct.unpack('=' + 'L', x)[0]
                    x = (x * h['xscale'] ) + h['xoffset']
                    if x < xmin or x > xmax:
                        continue

                    # test y point next ...
                    fh.seek(h['offset'] + (i * h['pointreclen'] + 4))
                    y = fh.read(4)
                    y = struct.unpack('=' + 'L', y)[0]
                    y = (y * h['yscale']) + h['yoffset']
                    if y < ymin or y > ymax:
                        continue

                    # extract round plot
                    if round and self.round_plot((x, y), centre_x, centre_y, extent_x) == 0:
                        continue

                    fh.seek(h["offset"] + (i * h['pointreclen'])) # searches to beginning of point

                    self.xy_dictionary["plot"] = self.write_point(self.xy_dictionary["plot"], self.extract_return(fh), h)

        if self.verbose: print "number of bad points = {}".format(self.badPoints)

        return self

    def tiles(self, resolution, take_sample=False, buffer=0):

        """
        Used to tile .las file

        Parameters
        ----------
        resolution: int
            Tile resolution

        take_sample: int, Default False
            Samples data at specified factor

        Returns
        -------
        out: self

        """

        if self.global_header["filesig"] == "ZNRF":
            raise NameError("ZNR is deprecated use an older version of lasIO")

        total_points = 0
        self.global_header["guid1"] = resolution

        xmax = (float(np.ceil(self.global_header["xmax"])) // resolution) * resolution # to make nice neat boxes!
        xmin = (float(np.floor(self.global_header["xmin"])) // resolution) * resolution
        ymax = (float(np.ceil(self.global_header["ymax"])) // resolution) * resolution
        ymin = (float(np.floor(self.global_header["ymin"])) // resolution) * resolution

        if self.tile == None:
            dirName = str(resolution) + "m_TILES"
        else:
            dirName = os.path.splitext(self.tile)[0] + "_" + str(resolution) + "m_TILES"
        self.savePath = os.path.join(self.savePath, dirName)

        # tile dictionary and other variables
        self.xy_dictionary = dict()
        for i, x in enumerate(np.arange(xmin, xmax + resolution, resolution)):
            for j, y in enumerate(np.arange(ymin, ymax + resolution, resolution)):
                self.xy_dictionary[(x, y)] = {"xmin":x - buffer,
                                              "ymin":y - buffer,
                                              "zmin":999,
                                              "xmax":x + resolution + buffer,
                                              "ymax":y + resolution + buffer,
                                              "zmax": -999,
                                              "num_rtn": {1:0, 2:0, 3:0, 4:0, 5:0},
                                              "i":0,
                                              "tempFile": os.path.abspath(os.path.join(self.tempDirectory, str(x) + "_" + str(y) + ".temp")),
                                              "outFile": os.path.abspath(os.path.join(self.savePath, str(x) + "_" + str(y) + ".las")),
                                              }

        self.keys = np.array(self.xy_dictionary.keys(), dtype=[('x', int), ('y', int)])
        if self.verbose: print "number of plots: {}".format(len(self.xy_dictionary))

        for tile in self.tile_list:
            
            if len(os.path.split(tile)) > 1:
                    tile = os.path.join(self.dir, tile)
    
            if self.copy:
                shutil.copyfile(tile, os.path.join(self.tempDirectory, os.path.split(tile)[1]))
                tile = os.path.join(self.tempDirectory, os.path.split(tile)[1])
            h = parseHeader(tile)
    
            if h["filesig"] == "LASF":
                if h["guid2"] == 1:
                    tile = self.laz2las(tile)
    
            if take_sample:
                sample = self.generateSample(take_sample, h)
            else:
                sample = range(h['numptrecords'])
    
            with open(os.path.join(self.dir, tile), 'rb') as fh:
    
                fh.seek(h["offset"])
    
                for i in sample: # loops through all points

                    fh.seek(h["offset"] + (i * h['pointreclen'])) # searches to beginning of point
    
                    point_dictionary = self.extract_return(fh)
                    X, Y = point_dictionary['x'], point_dictionary['y']
    
                    KEYS = self.keys[(X >= self.keys['x'] - buffer) & (X < self.keys['x'] + resolution + buffer) &
                                     (Y >= self.keys['y'] - buffer) & (Y < self.keys['y'] + resolution + buffer)]
    
                    for key in KEYS:
                        self.xy_dictionary[tuple(key)] = self.write_point(self.xy_dictionary[tuple(key)],
                                                                          point_dictionary, h)
    
                    # numPoints += 1
                    total_points += 1
    
                    if total_points % self.counter == 0 and self.verbose:
                        print "{:.1f}% | {} of {} points selected | {}".format((np.float(total_points)/self.global_header['numptrecords'])*100., total_points, self.global_header['numptrecords'], datetime.datetime.now())
    
                # deletes .las tiles that were converted from .laz
                if self.tempDirectory in tile:
                    os.unlink(tile)
                    
        return self

    def grid(self, csv, resolution=None, take_sample=False):

        """

        Returns a plotwise array of points from the tile defined with
        lasIO with plot centre at centre_x and centre_y and area equal
        to (radius*2)**2 if round=False and pi*radius**2 if round=True.

        The radius defaults to 10 metres. Plots are square by default
        but can be circular with round=True.

        Returns self which returns a numpy array if to_array is cploted,
        or can saved as .las, .txt or .xyz by cploting to_las,
        to_txt or to_xyz respectively.

        Paramaters
        ----------
        centre_x, centre_y : int or float
            Cartesian coordinates of plot centre (in the same coordinate
        system as data)

        extent : int, float or tuple with length of 2
            Diameter of round plot or extent of square plot.  Will except
            a tuple of two ints or floats.

        round : Boolean, defualt False
            If False a square plot is returned, if True a round plot is
        returned

        Returns
        -------
        out : self

        """

        take_sample = take_sample

        total_points = 0
        self.global_header["guid1"] = 0

        self.grid = np.loadtxt(csv, skiprows=1, delimiter=',', dtype=([('x', np.float), ('y', np.float)]))
        self.grid['x'] = self.grid['x'].astype(int)
        self.grid['y'] = self.grid['y'].astype(int)

        if not resolution:
            self.resolution = self.grid['x'][1] - self.grid['x'][0]
        else:
            self.resolution = resolution

        xmin = self.grid['x'].min() - (self.resolution / 2.)
        xmax = self.grid['x'].max() + (self.resolution / 2.)
        ymin = self.grid['y'].min() - (self.resolution / 2.)
        ymax = self.grid['y'].max() + (self.resolution / 2.)

        if self.tile is None:
            dir_name = str(self.resolution) + "m_GRID"
        else:
            dir_name = os.path.splitext(self.tile)[0] + "_" + str(self.resolution) + "m_GRID"
        self.savePath = os.path.join(self.savePath, dir_name)
        if os.path.isdir(self.savePath):
            shutil.rmtree(self.savePath)

        if self.verbose:
            print 'grid resolution:', self.resolution
            print 'aiming to produce {} tiles'.format(len(self.grid))

        self.xy_dictionary = dict()
        for x, y in self.grid:
            self.xy_dictionary[(x, y)] = {"xmin":x - (self.resolution / 2.),
                                          "ymin":y - (self.resolution / 2.),
                                          "zmin":999,
                                          "xmax":x + (self.resolution / 2.),
                                          "ymax":y + (self.resolution / 2.),
                                          "zmax": -999,
                                          "num_rtn": {1:0, 2:0, 3:0, 4:0, 5:0},
                                          "i":0,
                                          "outFile": os.path.abspath(os.path.join(self.savePath, "{}_{}.PLOT.las".format(x, y))),
                                          "tempFile": os.path.abspath(os.path.join(self.tempDirectory, "{}_{}.PLOT.temp".format(x, y)))}

        self.keys = np.array(self.xy_dictionary.keys(), dtype=[('x', int), ('y', int)])

        for tile in self.tile_list:

            if len(os.path.split(tile)) > 1:
                    tile = os.path.join(self.dir, tile)

            if self.copy:
                shutil.copyfile(tile, os.path.join(self.tempDirectory, os.path.split(tile)[1]))
                tile = os.path.join(self.tempDirectory, os.path.split(tile)[1])
            h = parseHeader(tile)

            if h["filesig"] == "LASF":
                if h["guid2"] == 1:
                    tile = self.laz2las(tile)

            if take_sample:
                sample = self.generateSample(take_sample, h)
            else:
                sample = range(h['numptrecords'])

            grid = self.grid[(self.grid['x'] >= h['xmin']) & (self.grid['x'] <= h['xmax']) &
                             (self.grid['y'] >= h['ymin']) & (self.grid['y'] <= h['ymax'])]

            with open(os.path.join(self.dir, tile), 'rb') as fh:

                fh.seek(h["offset"])

                for i in sample: # loops through all points

                    if total_points % self.counter == 0 and self.verbose:
                        print "{:.0f}% | {} of {} new tiles created | {}".format((total_points / float(self.global_header['numptrecords'])) * 100,
                                                                                 len(os.listdir(self.tempDirectory)), len(self.grid),
                                                                                 datetime.datetime.now())
                    total_points += 1

                    try:
                        # test x point first ...
                        fh.seek(h['offset'] + (i * h['pointreclen']))
                        x = struct.unpack('=' + 'L', fh.read(4))[0]
                        x = (x * h['xscale'] ) + h['xoffset']
                        if not xmin < x < xmax:
                            continue

                        # test y point next ...
                        fh.seek(h['offset'] + (i * h['pointreclen'] + 4))
                        y = struct.unpack('=' + 'L', fh.read(4))[0]
                        y = (y * h['yscale']) + h['yoffset']
                        if not ymin < y < ymax:
                            continue

                        # extract round plot
                        inGrid = False
                        idx = [(grid['x'] > h["xmin"]) | (grid['x'] <= h["xmax"]) |
                               (grid['y'] > h["ymin"]) | (grid['y'] <= h["ymax"])]
                        for row in grid[idx]:
                            if ((row[0] - (self.resolution / 2.)) < x < (row[0] + (self.resolution / 2.)) and
                                (row[1] - (self.resolution / 2.)) < y < (row[1] + (self.resolution / 2.))):
                                inGrid = True
                                break

                        if not inGrid: continue

                        fh.seek(h["offset"] + (i * h['pointreclen'])) # searches to beginning of point

                        point_dictionary = self.extract_return(fh)
                        X, Y = point_dictionary['x'], point_dictionary['y']
                        KEYS = self.keys[(X >= self.keys['x'] - (self.resolution / 2.)) & (X < self.keys['x'] + (self.resolution / 2.)) &
                                         (Y >= self.keys['y'] - (self.resolution / 2.)) & (Y < self.keys['y'] + (self.resolution / 2.))]

                        for key in KEYS:
                            self.xy_dictionary[tuple(key)] = self.write_point(self.xy_dictionary[tuple(key)], point_dictionary, h)

                    except:
                        self.badPoints += 1

                # deletes .las tiles that were converted from .laz or copies
                if self.tempDirectory in tile: os.unlink(tile)

        if self.resolution >= 1: h['guid1'] = self.resolution

        return self

    def write_point(self, tile, d, h):

        pointString = ''
        for i in self.point_format:
            if i[0] == 'return_grp':
                byte = ((d['scan_edge'] & 1) << 7) | ((d['scan_dir'] & 1) << 6) | \
                       ((d['rtn_tot'] & 7) << 3) | (d['rtn_num'] & 7)
            elif i[0] == 'x':
                byte = (d['x'] - h['xoffset']) / h['xscale']
            elif i[0] == 'y':
                byte = (d['y'] - h['yoffset']) / h['yscale']
            elif i[0] == 'z':
                byte = (d['z'] - h['zoffset']) / h['zscale']
            else:
                byte = d[i[0]]
            pointString += struct.pack('=' + i[2], byte)

        with open(tile["tempFile"], "ab") as o:
            o.write(pointString)

        # updates header information
        if d['z'] > tile["zmax"]:
            tile["zmax"] = d['z']
        if d['z'] < tile["zmin"]:
            tile["zmin"] = d['z']
        if 0 < d["rtn_num"] < 6:
            tile["num_rtn"][d["rtn_num"]] += 1
        tile["i"] += 1

        return tile

    def to_array(self, recarray=True):

        """
        Returns all points in a single array or as a list of
        arrays if there is more than one tile.  If there is
        more than one tile then it may be preferable to use
        to_dict command.
        """

        arr = []
        for i, key in enumerate(self.xy_dictionary.keys()):
            tile = self.xy_dictionary[key]
            if tile["i"] == 0:
                del tile
                continue
            a = np.zeros(tile["i"], dtype=self.dt)
            with open(tile["tempFile"], "rb") as fh:
                for j, line in enumerate(range(tile["i"])):
                    fh.seek(j * self.global_header['pointreclen'])
                    d = self.extract_return(fh)
                    for field in d:
                        a[j][field] = d[field]
            arr.append(a)
            if self.keep_temp is False: os.unlink(tile["tempFile"])

        if self.keep_temp is False: shutil.rmtree(self.tempDirectory)

        if len(arr) == 1:  # if list arr has only one array...
            arr = arr[0]   # ...return only the array
            if recarray: arr = arr.view(np.recarray)
        elif recarray:
            arr = [a.view(np.recarray) for a in arr]

        return arr

    def to_las(self, out=False):

        """
        Writes tile(s) to .las format.
        """

        if len(self.xy_dictionary) > 10:
            nTileCounter = len(self.xy_dictionary) // 10
        else:   nTileCounter = len(self.xy_dictionary)

        self.xy_dictionary = {key:values for key, values in self.xy_dictionary.items() if values['i'] > 0}

        if len(self.xy_dictionary) > 0:
            for i, key in enumerate(self.xy_dictionary.keys()):

                h = self.global_header
                if i % nTileCounter == 0 and i > 0 and self.verbose:
                    print "{:.0f}% | {} of {} tiles exported | {}".format(np.float(i) / len(self.xy_dictionary) * 100,
                                                                          i,
                                                                          len(self.xy_dictionary),
                                                                          datetime.datetime.now())

                tile = self.xy_dictionary[key]
                h['gensoftware'] = 'CRC207 LiDAR analysis software  '
                h['sysid'] = 'CRC207 LiDAR analysis software  '
                h['xmin'] = tile['xmin']
                h['xmax'] = tile['xmax']
                h['ymin'] = tile['ymin']
                h['ymax'] = tile['ymax']
                h['zmin'] = tile['zmin']
                h['zmax'] = tile['zmax']
                h['numptbyreturn'] = tuple([tile["num_rtn"][i] for i in range(1, 6)])
                h['numptrecords'] = tile["i"]
                h['guid2'] = 0
                if h['numvlrecords'] > 0:
                    h['offset'] = 313
                    h['numvlrecords'] = 1
                if self.resolution != None:
                    h['guid1'] = self.resolution

                if out:
                    if out.endswith('.las') and len(self.xy_dictionary) > 1:
                        outFile = out[:-4] + '.' + str(self.outTileCount) + '.las'
                    elif out.endswith('.las') and os.path.isdir(os.path.split(out)[0]):
                        outFile = out
                    elif os.path.isdir(out):
                        outFile = os.path.join(out, os.path.split(tile["outFile"])[1])
                    else:
                        raise IOError('out path not recognised')
                else:
                    if not os.path.isdir(self.savePath):
                        os.makedirs(self.savePath)
                    outFile = tile["outFile"]
                self.savePath = os.path.split(outFile)[0]

                with open(outFile, "wb") as outOpen:
                    for j in headerstruct():
                        if j[2] == 'c':
                            outOpen.write(h[j[0]])
                        elif j[3] > 1:
                            outOpen.write(struct.pack('=' + str(j[3]) + j[2], *h[j[0]]))
                        else:
                            outOpen.write(struct.pack('=' + j[2] , h[j[0]]))

                    # write VLR
                    if h['numvlrecords'] > 0:
                        # keeps only the first VLR e.g. the projection data
                        outOpen.write(self.vlr[:86])

                    # write points
                    outOpen.seek(h['offset'])
                    with open(tile["tempFile"], "rb") as o:
                        points = o.read()
                    outOpen.write(points)

                if self.keep_temp is False:  os.unlink(tile["tempFile"])

            print "100% | {} of {} tiles exported | {}".format(len(self.xy_dictionary),
                                                               len(self.xy_dictionary),
                                                               datetime.datetime.now())

            if len(self.xy_dictionary) == 1:
                print ".las file written to {}".format(outFile)
            else:
                print ".las file(s) written to {}".format(os.path.split(outFile)[0])
        else:
            print "! no tiles to export !"

        if not self.keep_temp:  shutil.rmtree(self.tempDirectory)

        return self

    def to_dict(self, recarray=False):

        """
        Returns array as a dictionary where the keys are
        the central coordinates and the values are a list of
        tuples (height, number of returns). This is useful
        for plotting or calculating continuous variables across
        a plot.
        """

        arr = {}
        for i, key in enumerate(self.xy_dictionary.keys()):
            tile = self.xy_dictionary[key]
            if tile["i"] == 0:
                del tile
                continue
            a = np.zeros(tile["i"], dtype = self.dt)
            with open(tile["tempFile"], "rb") as fh:
                for j, line in enumerate(range(tile["i"])):
                    fh.seek(j * self.global_header['pointreclen'])
                    d = self.extract_return(fh)
                    for field in d:
                        a[j][field] = d[field]
            arr[(tile["xmin"], tile["ymin"])] = a
            if not self.keep_temp:  os.unlink(tile["tempFile"])

        if not self.keep_temp:  shutil.rmtree(self.tempDirectory)

        return arr

    def to_txt(self, enum=False, outFile=False):

        for i, key in enumerate(self.xy_dictionary.keys()):
            tile = self.xy_dictionary[key]
            if tile["i"] == 0:
                del tile
                continue
            if enum:
                savePath = os.path.join(os.path.split(tile["outFile"])[0], "{}.txt".format(i))
            elif outFile:
                if os.path.isdir(outFile):
                    savePath = os.path.join(outFile, "{}.txt".format(i))
                else:
                    savePath = outFile
            else: savePath = os.path.splitext(tile["outFile"])[0] + ".txt"
            a = np.zeros(tile["i"], dtype = self.dt)
            with open(tile["tempFile"], "rb") as fh:
                for j, line in enumerate(range(tile["i"])):
                    fh.seek(j * self.global_header['pointreclen'])
                    d = self.extract_return(fh)
                    for field in d:
                        a[j][field] = d[field]
            np.savetxt(savePath, np.transpose([a['x'],
                                               a['y'],
                                               a['z'],
                                               a['class'],
                                               a['i'],
                                               a['scan_ang'],
                                               a['rtn_num'],
                                               a['rtn_tot']]),
                                               fmt='%.1f', delimiter=',')
            if self.verbose: print '.txt saved to:', savePath
            if self.keep_temp is False:  os.unlink(tile["tempFile"])

        if self.keep_temp is False:  shutil.rmtree(self.tempDirectory)

    def to_xyz(self):

        for i, key in enumerate(self.xy_dictionary.keys()):
            tile = self.xy_dictionary[key]
            if tile["i"] == 0:
                del tile
                continue
            self.savePath = os.path.splitext(tile["outFile"])[0] + ".txt"
            a = np.zeros(tile["i"], dtype = self.dt)
            with open(tile["tempFile"], "rb") as fh:
                for j, line in enumerate(range(tile["i"])):
                    fh.seek(j * self.global_header['pointreclen'])
                    d = self.extract_return(fh)
                    for field in d:
                        a[j][field] = d[field]
            np.savetxt(self.savePath, np.transpose([a['x'], a['y'], a['z']]), fmt='%.1f', delimiter=',')

            if self.keep_temp is False:  os.unlink(tile["tempFile"])

        if self.keep_temp is False:  shutil.rmtree(self.tempDirectory)

        if self.verbose: print 'XYZ saved to:', self.savePath

        return self

    def np2LAS(self, arr, out=False):

        """
        Can be used to export a numpy array in .las format
        """

        h = self.global_header

        h['gensoftware'] = 'CRC207 LiDAR analysis software  '
        h['sysid'] = 'CRC207 LiDAR analysis software  '
        h['xmin'] = arr['x'].min()
        h['xmax'] = arr['x'].max()
        h['ymin'] = arr['y'].min()
        h['ymax'] = arr['y'].max()
        h['zmin'] = arr['z'].min()
        h['zmax'] = arr['z'].max()
        ### sorting out the rtn_num tuple
        rtn = np.zeros(5).astype(int)
        for row in arr:
            rtn_num = row['rtn_num'] - 1
            if rtn_num < 5:
                rtn[rtn_num] += 1
        h['numptbyreturn'] = tuple(rtn)
        h['numptrecords'] = len(arr)
        h['guid2'] = 0

        if out:
            if out.endswith('.las'):
                outFile = out
            else:
                raise IOError('out path not recognised')
        else:
            x = str(np.mean(arr['x']).astype(int))
            y = str(np.mean(arr['y']).astype(int))
            outFile = os.path.join(self.savePath, "{}_{}.las".format(x, y))

        with open(outFile, "wb") as out:
            for j in headerstruct():
                if j[2] == 'c':
                    out.write(h[j[0]])
                elif j[3] > 1:
                    out.write(struct.pack('=' + str(j[3]) + j[2], *h[j[0]]))
                else:
                    out.write(struct.pack('=' + j[2] , h[j[0]]))

            ## write VLR
            if h['numvlrecords'] > 0:
                out.write(self.vlr)

            ## write points
            out.seek(h['offset'])
            for d in arr:
                for i in self.point_format:
                    if i[0] == 'return_grp':
                        byte = ((d['scan_edge'] & 1) << 7) | ((d['scan_dir'] & 1) << 6) | ((d['rtn_tot'] & 7) << 3) | (d['rtn_num'] & 7)
                    elif i[0] == 'x':
                        byte = (d['x'] - h['xoffset']) / h['xscale']
                    elif i[0] == 'y':
                        byte = (d['y'] - h['yoffset']) / h['yscale']
                    elif i[0] == 'z':
                        byte = (d['z'] - h['zoffset']) / h['zscale']
                    else:
                        byte = d[i[0]]
                    out.write(struct.pack('=' + i[2], byte))

        if self.verbose: print ".las file written to {}".format(outFile)

        if self.keep_temp is False:  shutil.rmtree(self.tempDirectory)

        return outFile

    def removeTemp(self):

        if os.path.isdir(self.tempDirectory):
            for file in os.listdir(self.tempDirectory):
                os.unlink(os.path.join(self.tempDirectory, file))

        shutil.rmtree(self.tempDirectory)
        if self.verbose: print "{} has been deleted".format(self.tempDirectory)

    def laz2las(self, tile):

        """
        If input file is a .laz file then calls a LAStools to
        decompress to .las. Requires LAStools to be installed and in
        the PATH to be in the system environment variables
        """

        if not np.all([True if 'lastools' in path else False for path in os.environ.items()]):
            raise Exception('It seems LAStools is not installed, if it is then add the \
                             path to the PATH environment variable')
        print "converting to .las: {}".format(os.path.abspath(tile))
        os.chdir(self.dir)
        tile_name = os.path.splitext(os.path.split(tile)[1])[0] + ".las"
        temp_file = os.path.join(self.tempDirectory, tile_name)
        cmd = ["las2las", "-i", tile, "-o", temp_file]
        subprocess.call(cmd)
        return temp_file

    def generateSample(self, sample, h):

        return np.random.choice(h['numptrecords'],
                                size=int(h['numptrecords'] / sample),
                                replace=False)

    def extract_return(self, fh):
    
        point_format = self.point_format
    
        point_dictionary = {} # dictionary for storing values in for each point
    
        for ent in point_format:
            byte = fh.read(ent[1])
            val = struct.unpack('=' + ent[2] , byte)[0]
            if ent[0] == 'x':
                val = (val * self.global_header['xscale'] ) + self.global_header['xoffset']
            if ent[0] == 'y':
                val = (val * self.global_header['yscale'] ) + self.global_header['yoffset']
            if ent[0] == 'z':
                val = (val * self.global_header['zscale'] ) + self.global_header['zoffset']
            if ent[0] == 'return_grp':
                point_dictionary['rtn_num'] = val & 7
                #if point_dictionary['rtn_num'] == 0:
                #    raise Exception
                point_dictionary['rtn_tot'] = (val >> 3) & 7
                point_dictionary['scan_dir'] = (val >> 6) & 1
                point_dictionary['scan_edge'] = (val >> 7)
                continue # required so that 'return_grp' is not added to dictionary
    
            point_dictionary[ent[0]] = val
    
        if point_dictionary["z"] > 1000000:
            raise NameError("z very high: {}".format(point_dictionary["z"]))
    
        return point_dictionary

    def getpoint_format(self):

        # return structure
        if "txt2las" in self.global_header["gensoftware"]:
            point_format, dt = point_fmtLTstruct()
        elif self.global_header["pointformat"] == 0:
            point_format, dt = point_fmt0struct()
        elif self.global_header["pointformat"] == 1:
            point_format, dt = point_fmt1struct()
        elif self.global_header["pointformat"] == 3:
            point_format, dt = point_fmt3struct()

        return point_format, dt

    def getVLR(self, las):
            fh = open(os.path.join(las))
            fh.seek(self.global_header["headersize"])
            vlr = fh.read(86)
            fh.close()
            return vlr

    def round_plot(self, point, plot_x, plot_y, r):

        x, y = point

        #NW
        if plot_x <= x and plot_y >= y:
            adj = x - plot_x
            opp = plot_y - y
            hyp = math.sqrt(math.pow(adj, 2) + math.pow(opp, 2))

        #SW
        elif plot_x <= x and plot_y <= y:
            adj = x - plot_x
            opp = y - plot_y
            hyp = math.sqrt(math.pow(adj, 2) + math.pow(opp, 2))

        #SE
        elif plot_x >= x and plot_y <= y:
            adj = plot_x - x
            opp = y - plot_y
            hyp = math.sqrt(math.pow(adj, 2) + math.pow(opp, 2))

        #NE
        elif plot_x >= x and plot_y >= y:
            adj = plot_x - x
            opp = plot_y - y
            hyp = math.sqrt(math.pow(adj, 2) + math.pow(opp, 2))

        if hyp > r: return 0

def parseHeader(filename):

    """
    returns header information as a dictionary.
    """

    with open(filename,'rb') as fh:
        header = {'infile':filename}

        if fh.read(4) == "ZNRF":
            raise Exception('ZNR is deprecated - use an older version of lasIO')
        else:   headerStructType = headerstruct()

        fh.seek(0)

        for i in headerStructType:
            if i[2] == 'c':
                value = fh.read(i[1])
            elif i[3] > 1:
                value = struct.unpack( '=' + str(i[3]) + i[2] , fh.read(i[1]) )
            else:
                value = struct.unpack( '=' + i[2] , fh.read(i[1]) )[0]
            header[i[0]] = value

    if headerStructType == headerstruct():
        if header["pointformat"] > 127:  # it is a .laz file
            header["pointformat"] -= 128
            header["numvlrecords"] -= 1
            header["offset"] -= 100
            header["guid2"] = 1

    return header


if __name__ == '__main__':

    # import shutil
    # path = '/Users/phil/ALS/WC/spl/tile_20/ForestLAS_tutorial/LAS/large_tile'
    # shutil.rmtree(os.path.join(path, 'WC45_SUB_20m_TILES'))
    # os.makedirs(os.path.join(path, 'WC45_SUB_20m_TILES'))
    # start = datetime.datetime.now()
    # # lasIO(os.path.join(path, 'WC45_SUB_5m_SUBSET'), verbose=True, number_of_processes=4).all().to_las()
    # lasIO(os.path.join(path, 'WC45_SUB.las'), verbose=True, number_of_processes=1).grid(os.path.join(path, 'coords_10.csv'), take_sample=20, resolution=10).to_las()
    # # lasIO(os.path.join(path, 'WC45_SUB_5m_SUBSET'), verbose=True, number_of_processes=8).tiling(1000).to_las(os.path.join(path, 'WC45_SUB_20m_TILES'))
    # print datetime.datetime.now() - start
    # LAS = lasIO(['/Users/phil/ALS/WC/spl/tile_20/WC1.las',
    #        '/Users/phil/ALS/WC/spl/tile_20/WC2.las'], verbose=True)
    LAS =  lasIO(['/Users/phil/ALS/WC/spl/tile_20/WC1.las'], verbose=True)
    print LAS.plot(LAS.x_centre, LAS.y_centre, 5, round=True).to_array()
    # print LAS.tiles(5, take_sample=20).to_dict()
    # LAS.all(take_sample=10)